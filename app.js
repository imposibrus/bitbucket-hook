
var express = require('express'),
    bodyParser = require('body-parser'),
    exec = require('child_process').exec,
    path = require('path'),
    app = express(),
    strikeFolderPath = path.resolve(__dirname, '..', 'strikeball2015');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(function(req, res) {
  console.log('incoming request with body:', require('util').inspect(req.body, {depth: null}));
  var data = JSON.parse(req.body.payload);

  console.log('hook for %s', data.repository.name);
  if(data.repository.name == 'strikeball2015') {
    console.log('is strike');
    exec('git reset --hard', {cwd: strikeFolderPath}, function(err, stdout, stderr) {
      if(err) {
        throw err;
      }
      if(stdout) {
        console.log(stdout);
      }
      if(stderr) {
        console.error(stderr);
      }

      exec('git pull origin master', {cwd: strikeFolderPath}, function(err, stdout, stderr) {
        if(err) {
          throw err;
        }
        if(stdout) {
          console.log(stdout);
        }
        if(stderr) {
          console.error(stderr);
        }

        exec('npm i', {cwd: strikeFolderPath}, function(err, stdout, stderr) {
          if(err) {
            throw err;
          }
          if(stdout) {
            console.log(stdout);
          }
          if(stderr) {
            console.error(stderr);
          }

          exec('sudo supervisorctl restart strike', function(err, stdout, stderr) {
            if(err) {
              throw err;
            }
            if(stdout) {
              console.log(stdout);
            }
            if(stderr) {
              console.error(stderr);
            }
          });
        });
      });
    });
  }

  res.end('');
});

var port = process.env.PORT || 3099;

app.listen(port, function() {
  console.log('server listening on port', port);
});
